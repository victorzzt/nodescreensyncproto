## For an art project in MscMET 20170426

* Not my project, however the linking of different tabs can be a good example for other projects
    * No time sync, so the responsiveness will be different according to different network latency
    * The protocol can be improved
* Developed in NodeJS + HTML CreateJS (soundjs + easeljs used)
* NodeJS using Express + socket.io
* Only broadcast, no strict timesync used
    * Wifi lag is significant
    * Mobile has problem loading multiple full resolution gif/video
    * Socket.io may lose packet now and then
* For future ,even if the library is irrelevant, the approach is still applicable
* Slobbish coding, need to refactor into tidy coding