// this includes contants or almost constants (fixed after initilization)
var scr_width_max = 800;
var scr_height_max = 1280;

var scr_width;
var scr_height;

// Everything is in this DIV
var all_div;

// Constants for loading assets
var imgNum = [6,6,6,6,6,    // there are 22 of the different img/sound lengths
              6,6,6,6,6,
              6,6,6,6,6,
              6,6,6,6,6,
              6,6];

// Image names
var imgPath = "videos/";
var imgPrefix = "GIF_";
var imgPostfix = ".gif";

// Audio
var audPath = "audios/"
var audPrefix = "MP3_";
var audPostfix = ".mp3";

// frame movement constants, might not need because of createjs
var last_step = 10.0;
var move_ratio = 0.3;

// tween length
var tween_length = 1000;