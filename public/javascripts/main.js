// Images
var img = [];

// sounds
var mp3s = [];
var mp3_clips = [];
var mp3_full_file = [];
var mp3_this_clip;
var mp3_next_clip;
var mp3_loaded;

// stores get value
var group_id;

// Socket object as global
var socket;

// ignore U touch
var touch_down_x;
var touch_down_y;

// lock responding if in animation
var lock_down;
var go_dir;
var current_clip;
var replace_clip;

// frames timer
var stage;
var socket_connected;

function htmlready() {
  // prepare div size
  prepare_divs();
  
  // new stage from canvas
  stage = new createjs.Stage("canvasMain");

  // lock_down
  socket_connected = false;
  lock_down = false;
  go_dir = -1;
  current_clip = 0; // this is the clip
  
  mp3_loaded = false;

  // search the get value
  var get_query = new URLSearchParams(window.location.search);
  group_id = get_query.get("id") - 1;
  if (group_id < 0) {
    group_id = 0;
  }
  if (group_id > 21) {
    group_id = 21;
  }
  console.log("Loading app number:" + group_id);

  addImgs();
  addAudios();
  // register mouse event
  register_mouse_event();
  
  createjs.Ticker.setFPS(25);
  createjs.Ticker.addEventListener("tick", frame_started);

  // Socket connect to server (post image since less traffic here)
  socket = io.connect();
  socket.on('update_dir', function(data) {
    var dir = parseInt(data.dir);
    start_move(dir);
    console.log("Received to move towards dir:" + dir);
  });
  socket.on('keep_alive', function(data) {
    socket_connected = true; // finally connected
    console.log('keep_alive:[' + JSON.stringify(data) + ']');
  });
  socket.on('disconnect', function(data) {
    socket_connected = false;   // disconnected
    console.log('Server is disconnected!');    
  });
}

function frame_started(e){
  var dt = e.delta; 
  stage.update(e);
}

function prepare_divs(){    // prepare div also set canvas size
  scr_width = screen.width;
  scr_height = screen.height;
  
  if( scr_width > scr_width_max ){
      scr_width = scr_width_max;
  }
  if( scr_height > scr_height_max ){
      scr_height = scr_height_max;
  }
  
  all_div = document.getElementById("divAll");
  all_div.style.width = ""+scr_width+"px";
  all_div.style.height = ""+scr_height+"px";
  
  var mainCanv = document.getElementById("canvasMain");
  mainCanv.style.width = ""+scr_width+"px";
  mainCanv.style.height = ""+scr_height+"px";
}

function start_move(dir) {
  if (lock_down) { // already moving
    return;
  }
  lock_down = true; // lock down, block all future command
  var all_clips = img.length;
  // east and south is current - 1, else current + 1
  var replace_clip_temp = current_clip + all_clips; // add a hole to prevent negative
  if (dir == 0 || dir == 2) {
    replace_clip_temp -= 1;
  } else {
    replace_clip_temp += 1;
  }
  go_dir = dir; // remember go dir;
  replace_clip_temp %= all_clips; // constrain to 0~(all_clips - 1)
  replace_clip = replace_clip_temp;
  console.log("Locked down, replacing with clip:" + replace_clip_temp);

  // prepare next clip
  var this_clip = img[current_clip];
  var next_clip = img[replace_clip];
  
  // prepare next sound
  if( mp3_loaded ){
    mp3_next_clip = mp3_clips[replace_clip];
    mp3_next_clip.play({interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
    mp3_next_clip.volume = 0.0;
  }
  
  // set next_clip visible
  next_clip.visible = true;
  
  // get start x y
  var source_next = [];
  var target_this = [];
  switch(go_dir){
  case 0:   // east place west
    source_next = [-scr_width, 0];
    target_this = [scr_width, 0];
  break;
  case 1:   // west place east
    source_next = [scr_width, 0];
    target_this = [-scr_width, 0];
  break;
  case 2:   // south place north
    source_next = [0, -scr_height];
    target_this = [0, scr_height];
  break;
  case 3:   // north place south
    source_next = [0, scr_height];
    target_this = [0, -scr_height];
  break;
  default:
    console.log("invalid dir, what happened?");
  break;
  }

  
  // create sound transition
  createjs.Tween.get(mp3_this_clip).to({volume: 0.0}, tween_length, createjs.Ease.linear); 
  createjs.Tween.get(mp3_next_clip).to({volume: 1.0}, tween_length, createjs.Ease.linear); 
  // create 2 image tweens
  if( go_dir == 0 || go_dir == 1 ){
    // make next clip in position
    next_clip.x = source_next[0];
    next_clip.y = source_next[1];
    next_clip.alpha = 1.0;  // needed if it's tweened to 0 in previous command
    console.log(source_next);
    console.log(target_this);    createjs.Tween.get(next_clip).to({x: 0, y: 0}, tween_length, createjs.Ease.quadOut).call(function(){console.log("Next in position");});   // no need to do anything
    createjs.Tween.get(this_clip).to({x: target_this[0], y: target_this[1]}, tween_length, createjs.Ease.quadOut).call(function(){console.log("This in position, unlocking");
                                                                                                                                  this_clip.visible = false;  // hide this clip
                                                                                                                                  current_clip = replace_clip;    // now this is the current clip
                                                                                                                                  // playmp3
                                                                                                                                    if( mp3_loaded ){
                                                                                                                                      mp3_this_clip.stop();
                                                                                                                                      mp3_this_clip = mp3_next_clip;
                                                                                                                                    }
                                                                                                                                  lock_down = false;  // unlock
                                                                                                                                  });
  }else{
    // make next clip transparent
    next_clip.x = 0;
    next_clip.y = 0;
    next_clip.alpha = 0.0;
    createjs.Tween.get(next_clip).to({alpha: 1.0}, tween_length, createjs.Ease.quadOut).call(function(){console.log("Next in position");});   // no need to do anything
    createjs.Tween.get(this_clip).to({alpha: 0.0}, tween_length, createjs.Ease.quadIn).call(function(){console.log("This in position, unlocking");
                                                                                                                                  this_clip.visible = false;  // hide this clip
                                                                                                                                  current_clip = replace_clip;    // now this is the current clip
                                                                                                                                  // playmp3
                                                                                                                                    if( mp3_loaded ){
                                                                                                                                      mp3_this_clip.stop();
                                                                                                                                      mp3_this_clip = mp3_next_clip;
                                                                                                                                    }
                                                                                                                                  lock_down = false;  // unlock
                                                                                                                                  });
  }
}

function addImgs() {    // stick with DOM cuz it still works
  var file_id = 1; // start from 1

  for (var i = file_id; i < file_id + imgNum[group_id]; i++) {
    // needs to be replaced with get id
    var load_name = imgPrefix + pad(group_id + 1, 2) + "_" + pad(i, 2) + imgPostfix;
    var cur_img = document.createElement("IMG");
    cur_img.setAttribute("src", imgPath + load_name);
    cur_img.style.width = "" + scr_width + "px";
    cur_img.style.height = "" + scr_height + "px";
    cur_img.style.position = "absolute";
    cur_img.style.top = 0;
    cur_img.style.left = 0;
    // add to body first
    all_div.appendChild(cur_img);
    
    // modification : img[] now saves createjs object instead
    var cj_img = new createjs.DOMElement(cur_img);
    if (i == file_id) {
        cj_img.x = 0;
        cj_img.visible = true;
    } else {
        cj_img.x = scr_width;
        cj_img.visible = false;
    }
    stage.addChild(cj_img);
    img.push(cj_img);
    console.log(imgPath + load_name + " loaded");
  }
  stage.update();
}

function addAudios() {
  var file_id = 1;
  
  // Init audio plugins
  if (!createjs.Sound.initializeDefaultPlugins()) {
    alert("Unsupported");
    return;
  }
  
  var audioPath = audPath;

  for (var i = file_id; i < file_id + imgNum[group_id]; i++) {
    // get mp3 id just like image
    var load_name = audPrefix + pad(group_id + 1, 2) + "_" + pad(i, 2) + audPostfix;
    mp3_full_file.push(audPath + load_name);
    mp3s.push( {id: "aud"+pad(i, 2), src:load_name} );
    console.log("Loading:"+ audPath + load_name);
  }
  
  createjs.Sound.addEventListener("fileload", sound_load);
  createjs.Sound.registerSounds(mp3s, audioPath);
}

function sound_load(e){
  var file_id = 1;
  // Let's do static instances for controlling
  for (var i = file_id; i < file_id + imgNum[group_id]; i++) {
    var mp3_instance = createjs.Sound.createInstance("aud"+pad(i,2))
    mp3_clips.push(mp3_instance);
    mp3_instance.volume = 0.0;
  }
  mp3_this_clip = mp3_clips[current_clip];
  mp3_this_clip.play({interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
  mp3_this_clip.volume = 1.0;

  mp3_loaded = true;
}

// add zeros to left , is used do not remove
function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function register_mouse_event() {
  // use document event instead of stage to prevent from dragging image
  // mouse clicked
  $(document).bind('touchstart mousedown',function(e) {
    e.preventDefault();
    touch_down_x = pointerEventToXY(e).x;
    touch_down_y = pointerEventToXY(e).y;
    // touch_down_x = e.pageX;
    // touch_down_y = e.pageY;
    // console.log("Down @" + touch_down_x + ","+touch_down_y);
  });

  // mouse released get gesture ( ignores U shaped touch for this prototype )
  $(document).bind('touchend mouseup',function(e) {
    // mouse released
    var touch_up_x = pointerEventToXY(e).x;
    var touch_up_y = pointerEventToXY(e).y;
    // console.log("Up @" + touch_up_x + ","+touch_up_y);
    // get direction
    var swipe_thresh = 30;
    var delta_x = touch_up_x - touch_down_x;
    var delta_y = touch_up_y - touch_down_y;
    var abs_x = Math.abs(delta_x);
    var abs_y = Math.abs(delta_y);
    if (abs_x > swipe_thresh || abs_y > swipe_thresh) { // need to be obvious enough touch 
      if (abs_x > abs_y) { // horiz
        if (delta_x > 0) { // east
          if (socket_connected) {
            console.log("Sending east");
            socket.emit('receive_dir', {
              dir: '0'
            });
          } else {
            console.log("Socket not ready, east not sent");
          }
        } else { // west
          if (socket_connected) {
            console.log("Sending west");
            socket.emit('receive_dir', {
              dir: '1'
            });
          } else {
            console.log("Socket not ready, west not sent");
          }
        }
      } else { // verti
        if (delta_y > 0) { // South
          console.log("Self South");
          start_move(2);
        } else { // North
          console.log("Self North");
          start_move(3);
        }
      }
    }
  });
}